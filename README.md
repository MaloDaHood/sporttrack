# SportTrack

## What is SportTrack ?

SportTrack is a web application that allows you to track your sport activities from your smart watch.

### How to use it ?

Login or create an account on the website.  
Then go to the upload page and upload your record file in the .JSON format.

### Where can I access it ?

The SportTrack website is accessible [here](https://malo-but.gitlab.io/s1/sporttrack).  
And the website's souce code is available [here](https://gitlab.com/malo-but/s1/sporttrack).

### Architecture

```text
sporttrack/
├── index.html
├── register.html
├── update.html
├── upload.html
├── error.html
├── redirect.html
├── README.md
├── css/ 
│   ├── style.css
|   └── index.html
|
└── assets/
    ├── logo.png
    ├── logo2.png
    ├── disconnect.png
    ├── update.png
    ├── upload.png
    └── index.html
```

```./``` contains the main files of the website.  
```./css/``` contains the css file.  
```./assets/``` contains the images used in the website.

### External resources used

- [W3 Markup Validation Service](https://validator.w3.org/#validate_by_input)
- [W3 CSS Validation Service](https://jigsaw.w3.org/css-validator/#validate_by_input)
- [pngFind](https://www.pngfind.com/)
- [Google Fonts](https://fonts.google.com/)
- [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/)
- [Grabient](https://cssgradient.io/gradient-backgrounds/)

### Comments

The current website is not fully responsive. It only fits basic 16:9 screens.  
css/ and assets/ folders are protected with automatic redirections to the main index page.

### Authors

- [Malo Massieu--Rocabois](https://gitlab.com/MaloDaHood)
- [Melen Le Jeune](https://gitlab.com/Melenlj)
